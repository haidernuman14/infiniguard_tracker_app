import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, Keyboard,AppState, Alert, KeyboardAvoidingView, TextInput, ScrollView } from 'react-native'
import { colors } from '../../Themes/color'
import Button from '../Common/Button'
import Header from '../Common/Header'
import I18n from '../../languages/i18n2'
import { launchCamera } from 'react-native-image-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import { moderateScale } from 'react-native-size-matters'
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoder';
import { CommonActions } from '@react-navigation/native'
import { Constant, WebServices } from '../../api/ApiRules'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Modal from 'react-native-modal';
var edit = require('../../Themes/Images/edit-pin.png')


const frame = require('../../Themes/Images/frame.png')

let images = []
let image_name = ['condenser_coil1', 'condenser_coil2', 'cabinet1', 'cabinet2', 'evaporator_coil1', 'evaporator_coil2']

export default class InspectionReviewScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            images: [],
            data: '',
            appState: AppState.currentState,
            isVisible: false,
            additionalImage1: '',
            additionalImage2: '',
            note: '',
            showModal: false,
        }
    }

    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
        var all_images = this.props.route.params.images
        images = all_images;
        var data = this.props.route.params.data
        this.setState({
            images: images,
            data: data
        })
        this.checkPermission()
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            this.checkPermission()
        }
        this.setState({ appState: nextAppState });
    }

    checkPermission() {
        if (Platform.OS == 'ios') {
            check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE).then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log('This feature is not available (on this device / in this context)');
                        break;
                    case RESULTS.DENIED:
                        request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE).then(result => {
                            console.log(result);
                            if (result == 'granted') {
                                this.setState({ permission: true }, () => { this.getLocation() })
                            }
                            if (result == 'blocked') {
                                Alert.alert('Enable Location', 'Please allow location for INFINIGUARD® from your phone setting screen.')
                            }
                        });
                        break;
                    case RESULTS.GRANTED:
                        this.setState({ permission: true }, () => { this.getLocation() })
                        break;
                    case RESULTS.BLOCKED:
                        Alert.alert('Enable Location', 'Please allow location for INFINIGUARD® App from your phone setting screen.')
                        break;
                }
            })
                .catch(error => {
                    console.log(error);
                });
        }
        if (Platform.OS == 'android') {
            check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log('This feature is not available (on this device / in this context)');
                        break;
                    case RESULTS.DENIED:
                        request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
                            console.log(result);
                            if (result == 'granted') {
                                this.setState({ permission: true }, () => { this.getLocation() })
                            }
                            if (result == 'blocked') {
                                Alert.alert('Enable Location', 'Please allow location for INFINIGUARD® from your phone setting screen.')
                            }
                        });
                        break;
                    case RESULTS.GRANTED:
                        this.setState({ permission: true }, () => { this.getLocation() })
                        break;
                    case RESULTS.BLOCKED:
                        Alert.alert('Enable Location', 'Please allow location for INFINIGUARD® App from your phone setting screen.')
                        break;
                }
            })
                .catch(error => {
                    console.log(error);
                });
        }
    }

    getLocation() {
        if (this.state.permission) {
            try {
                Geocoder.fallbackToGoogle('AIzaSyBX6rKXe6Jsk6ZynShEZiNfDfyhZWgmXsQ');
                Geolocation.getCurrentPosition(
                    (position) => {
                        var region = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude,
                        };
                        console.log("lat", region)
                        Geocoder.geocodePosition(region).then(res => {

                            var address = res[0].formattedAddress
                            this.setState({
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                                address: address,
                            });
                        })
                    },
                    (error) => console.log('error ' + error.message),
                    { enableHighAccuracy: false, timeout: 50000, maximumAge: 80000 }
                );
            }
            catch (err) {
                console.log(err);
            }
        }
    }

    selectPhotoTapped(index) {
        const options = {
            quality: 1.0,
            maxWidth: 800,
            maxHeight: 800,
            storageOptions: {
                skipBackup: true
            },
            multiple: false
        };
        launchCamera(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                images[index] = response.uri;
                this.setState({ images: images })
            }
        });
    }

    clickImage(index) {
        const options = {
            quality: 1.0,
            maxWidth: 800,
            maxHeight: 800,
            storageOptions: {
                skipBackup: true,
               
            },
            multiple: false
        };
        launchCamera(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                if (index == '1') {
                    this.setState({ additionalImage1: response.uri })
                }
                else if (index == '2') {
                    this.setState({ additionalImage2: response.uri })
                }
            }
        });
    }

    back = () => {
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name: 'InspectionPicScreen', params: { data: this.state.data } }],
        });
        this.props.navigation.dispatch(resetAction);
    }

    childCallback = () => {
        if (this.state.permission) {
            this.validationAndApiParameter("inspection_qr")
        }
        else {
            Alert.alert('Enable Location', 'Please allow location for INFINIGUARD® App from your phone setting screen.')
        }
    }

    validationAndApiParameter(apiname) {
        if (apiname == "inspection_qr") {
            const { images, data, address, note, latitude, longitude, additionalImage1, additionalImage2 } = this.state
            const data1 = new FormData();
            data1.append('qr_id', data.qr_id);
            data1.append('address', address);
            data1.append('lat', latitude);
            data1.append('lng', longitude);
            data1.append('notes', note);
            data1.append('additionalImage1', additionalImage1 == '' ? '' : {
                uri: additionalImage1,
                type: 'image/jpeg',
                name: 'additional1.jpg'
            });
            data1.append('additionalImage2', additionalImage2 == '' ? '' : {
                uri: additionalImage2,
                type: 'image/jpeg',
                name: 'additional2.jpg'
            });


            var image = ''
            for (let i = 0; i <= 1; i++) {
                image = images[i]
                if (images[i] != "") {
                    data1.append('equipment_pic' + i, {
                        uri: image,
                        type: 'image/jpeg',
                        name: image_name[i] + '.jpg'
                    });
                }
                else {
                    data1.append('equipment_pic' + i, '');
                }
            }
            console.log(data1);
            this.setState({ isVisible: true });
            this.postToApiCalling('POST', apiname, Constant.URL_qrInspection, data1);
        }
    }

    postToApiCalling(method, apiKey, apiUrl, data1) {
        new Promise(function (resolve, reject) {
            if (method == 'POST') {
                resolve(WebServices.callWebService(apiUrl, data1));
            } else {
                resolve(WebServices.callWebService_Get(apiUrl, data1));
            }
        }).then((jsonRes) => {
            this.setState({ isVisible: false })
            if ((!jsonRes) || (jsonRes.code == 0)) {
                setTimeout(() => { Alert.alert('Error', jsonRes.message); }, 200);
            } else {
                if (jsonRes.code == 1) {
                    this.apiSuccessfullResponse(apiKey, jsonRes)
                }
            }
        }).catch((error) => {
            console.log("ERROR" + error);
            this.setState({ isVisible: false })
            setTimeout(() => { Alert.alert("Server issue"); }, 200);
        });
    }

    async apiSuccessfullResponse(apiKey, jsonRes) {
        if (apiKey == 'inspection_qr') {
            var jdata = jsonRes.result
            images = []
            await AsyncStorage.setItem('inspection_id', JSON.stringify(jdata));
            this.props.navigation.navigate('InspectionThankyouScreen');
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Header naviagtion={this.props.navigation} />
                <ScrollView style={styles.sview}>
                    <View style={styles.container1}>
                        <Text allowFontScaling={false} style={styles.pageheading}>{I18n.t('InspectionReview.title1')}</Text>
                        <View style={styles.border} />
                        <Text allowFontScaling={false} style={styles.pageTitle}>{I18n.t('InspectionReview.title2')}</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        <View style={styles.container2}>
                            <View style={styles.buttonbox}>
                                <View style={styles.equipics}>
                                    <TouchableOpacity activeOpacity={.6} onPress={() => this.selectPhotoTapped(0)} style={styles.equipics1} >
                                        <Image style={styles.boxicon} source={{ uri: this.state.images[0] }} />
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={.6} onPress={() => this.selectPhotoTapped(1)} style={styles.equipics1} >
                                        <Image style={styles.boxicon} source={{ uri: this.state.images[1] }} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ marginVertical: moderateScale(10), alignItems: 'center', }} />

                                <View style={styles.equipics}>
                                    <TouchableOpacity activeOpacity={.6} onPress={() => this.clickImage('1')} style={styles.equipics1} >
                                        {this.state.additionalImage1 == '' ?
                                            <Image resizeMode='contain' style={styles.boxicon} source={frame} />
                                            :
                                            <Image style={styles.boxicon} source={{ uri: this.state.additionalImage1 }} />
                                        }
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={.6} onPress={() => this.clickImage('2')} style={styles.equipics1} >
                                        {this.state.additionalImage2 == '' ?
                                            <Image resizeMode='contain' style={styles.boxicon} source={frame} />
                                            :
                                            <Image style={styles.boxicon} source={{ uri: this.state.additionalImage2 }} />
                                        }
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                    {this.state.note.length == 0 ?
                        <View style={styles.bottomText}>
                            <TouchableOpacity style={{ borderColor: '#e1e1e1', borderWidth: moderateScale(1), padding: moderateScale(5) }} activeOpacity={0.8} onPress={() => this.setState({ showModal: true })}>
                                <Text style={styles.noteText}>{I18n.t('ReviewPicture.addNotes')}</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={{ flex: 1, padding: moderateScale(20) }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontSize: moderateScale(14), flex: 1, fontWeight: 'bold', color: '#000' }}>{I18n.t('ReviewPicture.note_tile')}</Text>
                                <TouchableOpacity onPress={() => this.setState({ showModal: true })} >
                                    <Image source={edit} style={{ width: moderateScale(12), height: moderateScale(12) }} />
                                </TouchableOpacity>
                            </View>
                            <Text style={{ fontSize: moderateScale(12), color: '#000', width: '100%' }}>{this.state.note}</Text>
                        </View>
                    }
                </ScrollView>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Button
                        style={{ width: '29.5%' }}
                        label={I18n.t('InspectionReview.back')}
                        parentCallback={this.back}
                    />
                    <Button
                        style={{ width: '70%' }}
                        label={I18n.t('InspectionReview.submit')}
                        parentCallback={this.childCallback}
                    />
                </View>
                <Modal
                    isVisible={this.state.showModal}
                    style={{ margin: 0 }}
                >
                    <TouchableOpacity activeOpacity={1} onPress={() => Keyboard.dismiss()} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <KeyboardAvoidingView behavior='padding' style={{ borderRadius: moderateScale(5), backgroundColor: '#e1e1e1', width: '90%', height: moderateScale(250) }}>
                            <Text style={{ width: '100%', textAlign: 'center', marginVertical: moderateScale(5), fontWeight: 'bold' }}>{I18n.t('ReviewPicture.header')}</Text>
                            <View style={{ flex: 1, padding: moderateScale(10) }}>
                                <TextInput
                                    allowFontScaling={false}
                                    autoCapitalize={'sentences'}
                                    autoFocus={true}
                                    maxLength={280}
                                    multiline
                                    value={this.state.note}
                                    numberOfLines={5}
                                    onChangeText={(text) => this.setState({ note: text })}
                                    placeholder={I18n.t('ReviewPicture.placeholder')}
                                    style={{ flex: 1, color: '#000', backgroundColor: '#fff', borderRadius: moderateScale(5) }}
                                    textAlignVertical='top'
                                />
                                <View style={{ width: '100%', alignItems: 'flex-end', paddingVertical: moderateScale(10) }}>
                                    <Text>{this.state.note.length}/280</Text>
                                </View>
                            </View>
                            <View style={{ width: '100%', height: moderateScale(40), alignItems: 'center', justifyContent: 'center', flexDirection: 'row', borderColor: '#999', borderTopWidth: moderateScale(1) }}>
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.setState({ showModal: false, note: this.state.note.trim() })}>
                                        <Text style={{ color: '#007AFF', fontWeight: '400' }}>{I18n.t('ReviewPicture.ok')}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </KeyboardAvoidingView>
                    </TouchableOpacity>
                </Modal>
                <Spinner visible={this.state.isVisible} />

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.colorWhite
    },
    container1: {
        paddingTop: moderateScale(20),
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: moderateScale(15),
        paddingHorizontal: moderateScale(5),
    },
    border: {
        width: moderateScale(30),
        borderBottomWidth: moderateScale(2),
        color: '#13b0dd',
        margin: moderateScale(1)
    },
    pageheading: {
        fontSize: moderateScale(18),
        textAlign: 'center',
        color: colors.loginPrimary,
        width: '100%',
        fontWeight: 'bold'
    },
    pageTitle: {
        fontSize: moderateScale(14),
        paddingTop: moderateScale(5),
        fontWeight: '700',
        textAlign: 'center',
        width: '100%',
    },
    container2: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
    },
    buttonbox: {
        width: '90%',
        marginBottom: moderateScale(15),
    },
    equipics: {
        height: moderateScale(150),
        width: '48%',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    equipics1: {
        flexDirection: 'column',
        width: '100%',
        marginRight: moderateScale(8),
    },
    boxicon: {
        height: '100%',
        width: '100%',
    },
    noteText: {
        fontSize: moderateScale(14)
    },
    bottomText: {
        width: '100%',
        paddingHorizontal: moderateScale(20),
        height: moderateScale(60),
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    noteText: {
        fontSize: moderateScale(14)
    },
})