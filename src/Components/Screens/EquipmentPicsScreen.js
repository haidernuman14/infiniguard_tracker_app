import React, { Component } from 'react';
import {StyleSheet,Text,View,Alert,Dimensions,Vibration} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { RNCamera } from 'react-native-camera';
import Header from '../Common/Header'
import Button from '../Common/Button'
import I18n from '../../languages/i18n2';
import { moderateScale } from 'react-native-size-matters';
import SystemSetting from 'react-native-system-setting'

let images=[]
let count=0;

export default class EquipmentPicsScreen extends Component {
    constructor (props) {
        super(props)
        this.state = {
            isVisible: false,
            images:[],
            data1:'',
            count:0,
            totalcount:0,
            title:''
        }
    }

    componentDidMount() {
        console.log(this.props.route.params.data);
        let data=this.props.route.params.data
        this.setState({data1 : data})
        this.showAlert(data)
        for(let i=0;i<=1;i++){
            images[i]=''
        }
    }

    showAlert(){
        this.setState({totalcount : 2, title:I18n.t('PicScreen.title1')})
        Alert.alert(I18n.t('PicScreen.alert_head'),I18n.t('PicScreen.alert_text'))
    }

    back = () => {
        this.props.navigation.pop();
    }

    childCallback = async () => {
        SystemSetting.getVolume().then(currentVolume => {
            if(currentVolume==0){
            Vibration.vibrate(100)
            }
        });
        if (this.camera) {
            let width= Dimensions.get('window').width
            let height= Dimensions.get('window').height-moderateScale(100)
            const options = {
                width:width,
                height:height,
                quality: 0.5,
                mirrorImage:false,
                orientation:'portrait'
            };
            const data = await this.camera.takePictureAsync(options)
            let {data1,totalcount}=this.state
            this.setState({title:I18n.t('PicScreen.title2')})
            images[count]=data.uri
            count= count + 1;
            if(count==totalcount){
                this.setState({images: images});
                images=[];
                count=0;
                this.props.navigation.navigate('ReviewPicture',{images: this.state.images,data: data1});
            }
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <Header navigation={this.props.navigation}/>
                <View style={styles.container1}>
                    <Text allowFontScaling={false}  style={styles.pageheading}>{this.state.title}</Text>
                </View>
                <View style={styles.container2}>
                    <RNCamera
                    ref={ref => {
                        this.camera = ref;
                     }}
                    captureAudio={false}
                    style = {styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.off}
                    />
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',position:'absolute',bottom:0,backgroundColor:"white"}}>
                    <Button
                        style={{width:'29.5%'}}
                        label={I18n.t('PicScreen.back')}
                        parentCallback={this.back}
                    />
                    <Button
                        style={{width:'70.3%'}}
                        label={I18n.t('PicScreen.takePic')}
                        parentCallback={this.childCallback}
                    />
                </View>
                <Spinner visible={this.state.isVisible}  />
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black'
  },
  container2:{
    flex:1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  container1:{
    backgroundColor:'white',
    height:moderateScale(60),
    justifyContent:'center',
    alignItems:'center',
   
  },
  pageheading:{
    fontSize:moderateScale(14),
    fontWeight:'700',
    textAlign:'center',
    color: '#32c8ed',
    width:"100%",

  },
});
