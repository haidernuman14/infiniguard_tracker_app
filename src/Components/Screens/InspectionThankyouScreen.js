import React, { Component } from 'react';
import {StyleSheet,Text,View} from 'react-native';
import Header from '../Common/Header'
import Button from '../Common/Button'
import I18n from '../../languages/i18n2';
import { moderateScale } from 'react-native-size-matters';
import { CommonActions } from '@react-navigation/native';

export default class InspectionThankyouScreen extends Component {
    constructor (props) {
        super(props)
        this.state = {
        isVisible: false,
        }
    }

  warrantyclaim=()=>{
    this.props.navigation.navigate('WarrantyClaimScreen');
  }

  additionalcomponentbtn=()=>{
    const resetAction = CommonActions.reset({
        index: 0,
        routes: [{ name:'ScanQRCodeScreen'}],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <View style={styles.container}>
        <Header navigation={this.props.navigation}/>
        <View style={styles.container1}>
          <Text allowFontScaling={false}  style={styles.thanktext}>
            {I18n.t('InspectionThankyou.Text1')}{'\n'}{'\n'}
            {I18n.t('InspectionThankyou.Text2')}
          </Text>
        </View>
        <View style={{flexDirection:'row',justifyContent:'space-between',position:'absolute',bottom:0}}>
        <Button
            style={{width:'54.6%'}}
            label={I18n.t('InspectionThankyou.Inspect')}
            parentCallback={this.additionalcomponentbtn}
        />
        <Button
            style={{width:'44.9%'}}
            label={I18n.t('InspectionThankyou.Submit')}
            parentCallback={this.warrantyclaim}
        />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container1:{
    alignItems:'center',
    flex:3,
    paddingHorizontal:moderateScale(5),
    paddingTop:moderateScale(20),
    paddingLeft:30,
    paddingRight:30
  },
  thanktext:{
    fontSize:moderateScale(15),
    fontWeight:'300',
    color:'#0f0f0f',
    textAlign:'left',
  },
});
