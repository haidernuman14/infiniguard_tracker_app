import React, { Component } from 'react';
import {StyleSheet,Text,View} from 'react-native';
import Header from '../Common/Header'
import Button from '../Common/Button';
import I18n from '../../languages/i18n2';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Fonts } from '../../utils/Fonts';
import { moderateScale } from 'react-native-size-matters';
import { CommonActions } from '@react-navigation/native';

export default class ThankyouScreen extends Component {
    constructor (props) {
        super(props)
        this.state = {
            isVisible: false,
            seller_name:null,
            qr_code:''
        }
    }

    componentWillMount() {
        AsyncStorage.getItem('UserData').then((UserData) => {
            const data = JSON.parse(UserData)
            this.setState({ seller_name:data.name })
        })
        AsyncStorage.getItem('qr_code').then((qr_code) => {
            const code = JSON.parse(qr_code)
            this.setState({ qr_code: code })
        })
    }

    componentbtn=()=>{
        this.props.navigation.navigate('RegisterQRCodeScreen');
    }

    logoutbtn=async ()=>{
        const keys = ['loggedIn', 'UserData', 'qr_code', 'qr_id']
        await AsyncStorage.multiRemove(keys);
        const resetAction = CommonActions.reset({
            index: 0,
            routes: [{ name:'ScanQRCodeScreen'}],
        });
        this.props.navigation.dispatch(resetAction);
    }


    render() {
        return (
            <View style={styles.container}>
                <Header navigation={this.props.navigation} />
                <View style={styles.container1}>
                    <Text allowFontScaling={false} style={styles.headtitle}>{I18n.t('REGISTRATION_COMPLETE.Title')}</Text>
                    <View style={styles.border} />
                    <Text allowFontScaling={false} style={styles.thanktext}>
                        {I18n.t('REGISTRATION_COMPLETE.Thankyou')+" "+this.state.seller_name} <Text style={{fontWeight:'bold',fontSize:moderateScale(16)}}>{this.state.seller_name}</Text> {I18n.t('REGISTRATION_COMPLETE.Text')}
                    </Text>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <Button
                        style={{width:'29.3%'}}
                        label={I18n.t('REGISTRATION_COMPLETE.logout')}
                        parentCallback={this.logoutbtn}
                    />
                    <Button
                        style={{width:'70%'}}
                        label={I18n.t('REGISTRATION_COMPLETE.Register')}
                        parentCallback={this.componentbtn}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    flex: 1,
    backgroundColor: '#fff',
    height:'100%',
  },
  container1:{
    flex:1,
    alignItems:'center',
    paddingTop:moderateScale(10)
  },
  thanktext:{
    fontSize:moderateScale(16),
    fontWeight:'400',
    color:'#0f0f0f',
    textAlign:'center',
    // fontFamily:Fonts.RobotoRegular,
    paddingHorizontal:moderateScale(20),
    paddingVertical:moderateScale(10),
  },
  headtitle:{
    fontSize:moderateScale(18),
    marginTop:moderateScale(5),
    fontWeight:'700',
    color:'#04bdf0',
    width:'100%',
    textAlign:'center',
    // fontFamily:Fonts.RobotoBlack
  },
  border:{
    width:moderateScale(50),
    borderBottomWidth:moderateScale(2),
    color:'#000',
    margin:moderateScale(15)
  },
});
